/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.impl;

import adx.syncReToOrcl.sqlConnector.api.DatabaseManager;
import adx.syncReToOrcl.sqlConnector.api.Insertor;
import adx.syncReToOrcl.sqlConnector.api.Selector;
import adx.syncReToOrcl.sqlConnector.exception.DriverNotFoundException;
import adx.syncReToOrcl.sqlConnector.util.DatabaseDescription;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 *
 * @author root
 */
public class DatabaseManagerImpl implements DatabaseManager
{

    private HashMap<String, ComboPooledDataSource> dataSources;
    private int insertBatchSize;

    public final void setBatchSize(int insertBatchSize)
    {
        this.insertBatchSize = insertBatchSize;
    }

    @Override
    public Selector createSelector(String databaseLabel)
    {
        ComboPooledDataSource dataSource = dataSources.get(databaseLabel);
        if (dataSource == null)
        {
            return null;
        }

        Selector selector = null;
        SelectorImpl selectorImpl = new SelectorImpl();
        selectorImpl.setDataSource(dataSource);
        selector = selectorImpl;

        return selector;
    }
    

	@Override
	public Insertor createInsertor(String databaseLabel) {
		ComboPooledDataSource dataSource = dataSources.get(databaseLabel);
        if (dataSource == null)
        {
            return null;
        }

        Insertor insertor = null;
        InsertorImpl insertorImpl = new InsertorImpl();
        insertorImpl.setDataSource(dataSource);
        insertor = insertorImpl;

        return insertor;
	}

    @Override
    public String connect(DatabaseDescription databaseDescription, String dbType) throws DriverNotFoundException
    {
        String databaseId = databaseDescription.getJdbcUrl();
        ComboPooledDataSource dataSource = dataSources.get(databaseId);

        if (dataSource == null)
        {
            dataSource = new ComboPooledDataSource();

            // configuration for dataSource
            dataSource.setUser(databaseDescription.getUser());
            dataSource.setPassword(databaseDescription.getPassword());
            dataSource.setJdbcUrl(databaseId);
            dataSource.setMinPoolSize(databaseDescription.getMinPoolSize());
            dataSource.setMaxPoolSize(databaseDescription.getMaxPoolSize());
            dataSource.setAcquireIncrement(databaseDescription.getAcquireIncrement());
            dataSource.setMaxStatements(databaseDescription.getMaxStatements());

            if (dbType.equals("mysql")) {
            	try
                {
                    dataSource.setDriverClass("com.mysql.jdbc.Driver");
                    dataSource.setPreferredTestQuery("SELECT 1");
                    dataSource.setIdleConnectionTestPeriod(3600);
                } catch (PropertyVetoException ex)
                {
                    throw new DriverNotFoundException(ex);
                }
			} else if (dbType.equals("oracle")) {
				try
	            {
	                dataSource.setDriverClass("oracle.jdbc.driver.OracleDriver");
	                dataSource.setPreferredTestQuery("SELECT 1");
	                dataSource.setIdleConnectionTestPeriod(3600);
	            } catch (PropertyVetoException ex)
	            {
	                throw new DriverNotFoundException(ex);
	            }
			}
            
            

            dataSources.put(databaseId, dataSource);

        }

        return databaseId;
    }

    @Override
    public void disconnect(String databaseLabel)
    {
        ComboPooledDataSource dataSource = dataSources.get(databaseLabel);
        if (dataSource != null)
        {
            try
            {
                DataSources.destroy(dataSource);
            } catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void shutdown()
    {
        for (ComboPooledDataSource dataSource : dataSources.values())
        {
            try
            {
                DataSources.destroy(dataSource);
            } catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }

        dataSources.clear();
    }

    public void initiate()
    {
        this.dataSources = new HashMap<String, ComboPooledDataSource>();
    }
    
}
