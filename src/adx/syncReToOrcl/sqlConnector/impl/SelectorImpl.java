/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import adx.syncReToOrcl.sqlConnector.api.Selector;
import adx.syncReToOrcl.sqlConnector.util.Common;
import adx.syncReToOrcl.sqlConnector.util.DataStructure;

/**
 *
 * @author LamHV
 */
public class SelectorImpl implements Selector
{

    private DataSource dataSource;
    private Logger systemLogger = LoggerFactory.getLogger("system.db.SelectorImpl: creating Selector");

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    @Override
    public ArrayList<ArrayList<String>> selectAllFields(String query, String tableName)
    {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        ArrayList<String> fields = Common.getFields(tableName);
        int numOfFields = fields.size();

        this.systemLogger.info("Query: " + query);

        Connection connection;
        try
        {
            connection = dataSource.getConnection();
            // Disable auto-commit
            connection.setAutoCommit(false);

            // Set savepoint 
            Savepoint savepoint = connection.setSavepoint();

            // Execute and configura following streamming
            PreparedStatement preparedStatement;
            try
            {
                preparedStatement = connection.prepareStatement(query,
                        ResultSet.TYPE_FORWARD_ONLY,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next())
                {
                    ArrayList<String> row = new ArrayList<String>(numOfFields);
                    for (int i = 0; i < numOfFields; i++)
                    {
                        row.add(resultSet.getString(i + 1));
                    }
                    result.add(row);
                }

                connection.commit();
            } catch (SQLException ex)
            {
                systemLogger.debug(ex.getMessage());
                systemLogger.debug("Roll back");
                connection.rollback(savepoint);
                systemLogger.debug("Roll back done");
                throw ex;
            } finally
            {
                connection.setAutoCommit(true);
                connection.close();
            }

        } catch (SQLException ex)
        {
            systemLogger.debug(ex.getMessage());
        }

        return result;
    }

    private ArrayList<ArrayList<String>> selectSpecificFields(String query, int numOfFields)
    {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

        this.systemLogger.info("Query: " + query);

        Connection connection;
        try
        {
            connection = dataSource.getConnection();
            // Disable auto-commit
            connection.setAutoCommit(false);

            // Set savepoint 
            Savepoint savepoint = connection.setSavepoint();

            // Execute and configura following streamming
            PreparedStatement preparedStatement;
            try
            {
                preparedStatement = connection.prepareStatement(query,
                        ResultSet.TYPE_FORWARD_ONLY,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next())
                {
                    ArrayList<String> row = new ArrayList<String>(numOfFields);
                    for (int i = 0; i < numOfFields; i++)
                    {
                        row.add(resultSet.getString(i + 1));
                    }
                    result.add(row);
                }

                connection.commit();
            } catch (SQLException ex)
            {
                systemLogger.debug(ex.getMessage());
                systemLogger.debug("Roll back");
                connection.rollback(savepoint);
                systemLogger.debug("Roll back done");
                throw ex;
            } finally
            {
                connection.setAutoCommit(true);
                connection.close();
            }

        } catch (SQLException ex)
        {
            systemLogger.debug(ex.getMessage());
        }

        return result;
    }

    @Override
    public ArrayList<DataStructure> getHardId(String id, String tableName, int type)
    {
        // Get questions
        String questionsTable = tableName + "_questions";
        String realQuestionsTable = "";
        String realTable = "";
        String query = "";
        ArrayList<DataStructure> results = new ArrayList<DataStructure>();        
        
        if (type == 0)
        {
            realQuestionsTable = questionsTable;
            realTable = tableName;
            query = String.format("select * from %s where %s = %s", realQuestionsTable, "id", id);
            
            DataStructure myData = new DataStructure();
            
            // questionsTable: Do not config and remove field id_realtest
            myData.setRows(selectAllFields(query, questionsTable));

            // Get id, transcript, audioUrl
            query = String.format("select * from %s where %s = %s", tableName, "id", id);

            ArrayList<ArrayList<String>> rows = selectAllFields(query, tableName);
            for (int i = 0; i < rows.size(); i++)
            {
                for (int j = 0; j < rows.get(i).size(); j++)
                {
                    switch (j)
                    {
                        case 0:
                            myData.setId(id);
                            break;
                        case 1:
                            myData.setTranscript(rows.get(i).get(j));
                            break;
                        case 2:
                            myData.setAudioUrl(rows.get(i).get(j));
                            break;
                    }
                }
            }
            results.add(myData);
            
        } else
        {
            realQuestionsTable = "realtest_" + tableName + "_questions";
            realTable = "realtest_" + tableName;
            
            // Get id, transcript, audioUrl
            query = String.format("select * from %s where %s = %s", realTable, "id_realtest", id);
            
            ArrayList<ArrayList<String>> rows = selectAllFields(query, tableName);
            for (int i = 0; i < rows.size(); i++)
            {
                DataStructure myData = new DataStructure();
                for (int j = 0; j < rows.get(i).size(); j++)
                {
                    switch (j)
                    {
                        case 0:
                            myData.setId(rows.get(i).get(j));
                            // Get questions
                            query = String.format("select * from %s where %s = %s", 
                                    realQuestionsTable, 
                                    "id", 
                                    rows.get(i).get(j));
                            myData.setRows(selectAllFields(query, questionsTable));
                            break;
                        case 1:
                            myData.setTranscript(rows.get(i).get(j));
                            break;
                        case 2:
                            myData.setAudioUrl(rows.get(i).get(j));
                            break;
                    }
                }
                results.add(myData);                
            }
        }
        
        return results;
    }

    @Override
    public ArrayList<ArrayList<String>> getEasyId(String id, String tableName, int type)
    {
        String query = "";
        // practice
        if (type == 0)
        {
            query = String.format("select * from %s where %s = %s", tableName, "id", id);

        } else // download
        {
            String realTableName = "realtest_" + tableName;
            query = String.format("select * from %s where %s = %s",
                    realTableName,
                    "id_realtest",
                    id);
        }

        return selectAllFields(query, tableName);
    }

    @Override
    public ArrayList<ArrayList<String>> getRandomId(ArrayList<String> ids, String tableName)
    {
        String listId = "";
        for (int i = 0; i < ids.size() - 1; i++)
        {
            listId += ids.get(i) + ", ";
        }
        listId += ids.get(ids.size() - 1);

        String query = "";
        // Get random ids
        if (tableName.equals("section1"))
        {
            query = String.format("select %s from %s where %s not in (%s) ORDER BY RAND() limit 10", "id", tableName, "id", listId);
        } else if (tableName.equals("section2") || tableName.equals("section3") || tableName.equals("section4"))
        {
            query = String.format("select %s from %s where %s not in (%s) ORDER BY RAND() limit 30", "id", tableName, "id", listId);
        } else if (tableName.equals("section5"))
        {
            query = String.format("select %s from %s where %s not in (%s) ORDER BY RAND() limit 40", "id", tableName, "id", listId);
        } else if (tableName.equals("section6"))
        {
            query = String.format("select %s from %s where %s not in (%s) ORDER BY RAND() limit 12", "id", tableName, "id", listId);
        } else if (tableName.equals("section7"))
        {
            query = String.format("select %s from %s where %s not in (%s) ORDER BY RAND() limit 48", "id", tableName, "id", listId);
        }

        return selectSpecificFields(query, 1);
    }

    @Override
    public ArrayList<ArrayList<String>> getImageId(String id, String tableName)
    {
        String query = String.format("select %s from %s where %s = %s", "imageUrl", tableName, "id", id);
        return selectSpecificFields(query, 1);
    }

    @Override
    public ArrayList<ArrayList<String>> getAudioId(String id, String tableName)
    {
        String query = String.format("select %s from %s where %s = %s", "audioUrl", tableName, "id", id);
        return selectSpecificFields(query, 1);
    }

    @Override
    public ArrayList<ArrayList<String>> countRealTest()
    {
        String query = String.format("select * from %s", "realtest_management");
        return selectAllFields(query, "realtest_management");
    }

    @Override
    public ArrayList<ArrayList<String>> getRealTest(String tableName, String id)
    {
        String query = "";
        String realTableName = "realtest_" + tableName;
        if (tableName.equals("section1"))
        {
            query = String.format("select * from %s where %s = %s limit 10",
                    realTableName,
                    "id_realtest",
                    id);
            return selectAllFields(query, tableName);
        } else if (tableName.equals("section2"))
        {
            query = String.format("select * from %s where %s = %s limit 30", realTableName, "id_realtest", id);
            return selectAllFields(query, tableName);
        } else if (tableName.equals("section3"))
        {
            query = String.format("select a3.*, b3.*"
                    + "from (SELECT a2.*, b2.* "
                    + "FROM ("
                    + "SELECT a1.id as master_id, a1.transcript, a1.id2, a1.id3, b1.id as tmp_id, "
                    + "b1.question as tmp_question, b1.choiceA as tmp_choiceA, "
                    + "b1.choiceB as tmp_choiceB, b1.choiceC as tmp_choiceC, "
                    + "b1.choiceD as tmp_choiceD, b1.solution as tmp_solution, "
                    + "b1.id_realtest as tmp_id_realtest "
                    + "FROM %s AS a1 "
                    + "JOIN %s AS b1 ON a1.id1 = b1.id "
                    + "AND a1.id_realtest = b1.id_realtest "
                    + "WHERE a1.id_realtest = %s ) AS a2 "
                    + "JOIN %s AS b2 ON a2.id2 = b2.id "
                    + "where b2.id_realtest = %s ) as a3 "
                    + "JOIN %s AS b3 ON a3.id3 = b3.id "
                    + "where b3.id_realtest = %s limit 10;", "realtest_section3",
                    "realtest_section3_questions", id, "realtest_section3_questions", id,
                    "realtest_section3_questions", id);
            return selectSpecificFields(query, 28);
        } else if (tableName.equals("realtest_section4"))
        {
            query = String.format("select a3.*, b3.*"
                    + "from (SELECT a2.*, b2.* "
                    + "FROM ("
                    + "SELECT a1.id as master_id, a1.transcript, a1.id2, a1.id3, b1.id as tmp_id, "
                    + "b1.question as tmp_question, b1.choiceA as tmp_choiceA, "
                    + "b1.choiceB as tmp_choiceB, b1.choiceC as tmp_choiceC, "
                    + "b1.choiceD as tmp_choiceD, b1.solution as tmp_solution, "
                    + "b1.id_realtest as tmp_id_realtest "
                    + "FROM %s AS a1 "
                    + "JOIN %s AS b1 ON a1.id1 = b1.id "
                    + "AND a1.id_realtest = b1.id_realtest "
                    + "WHERE a1.id_realtest = %s ) AS a2 "
                    + "JOIN %s AS b2 ON a2.id2 = b2.id "
                    + "where b2.id_realtest = %s ) as a3 "
                    + "JOIN %s AS b3 ON a3.id3 = b3.id "
                    + "where b3.id_realtest = %s limit 10;", "realtest_section4",
                    "realtest_section4_questions", id, "realtest_section4_questions", id,
                    "realtest_section4_questions", id);
            return selectSpecificFields(query, 28);
        }

        return null;
    }
}
