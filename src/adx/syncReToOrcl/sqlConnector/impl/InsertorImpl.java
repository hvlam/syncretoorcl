package adx.syncReToOrcl.sqlConnector.impl;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import adx.syncReToOrcl.processor.DataStructure;
import adx.syncReToOrcl.sqlConnector.api.Insertor;

public class InsertorImpl implements Insertor {
	private DataSource dataSource;
	private Integer insertBatchSize;
	private static Logger logger = LoggerFactory
			.getLogger("system.db.InsertorImpl: creating Insertor");

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void setInsertBatchSize(Integer insertBatchSize) {
		this.insertBatchSize = insertBatchSize;
	}

	private String buildInsertStatement(String table, int fieldAmount,
			ArrayList<String> fields) {
		// Insert statement pattern: INSERT INTO <t> [(<f1>,..,<fn>)] VALUES
		// (?,..,?)

		String questionMaskString = "";
		if (fields != null) {
			String fieldString = "";
			for (int i = 0; i < fieldAmount - 1; i++) {
				fieldString += String.format("%s, ", fields.get(i));
				questionMaskString += "?, ";
			}
			fieldString += fields.get(fieldAmount - 1);
			questionMaskString += "?";

			return String.format("INSERT INTO %s (%s) VALUES (%s)", table,
					fieldString, questionMaskString);
		} else {
			for (int i = 0; i < fieldAmount - 1; i++) {
				questionMaskString += "?, ";
			}
			questionMaskString += "?";

			return String.format("INSERT INTO %s VALUES (%s)", table,
					questionMaskString);
		}
	}
	
	private String buildCallableStatement(String spName, int amountParameters) {
		String questionMaskString = "";
		for (int i = 0; i < amountParameters; i++) {
			if (i == 0) {
				questionMaskString = "?";
			} else {
				questionMaskString += ",?";
			}
		}
		
		String callSP = String.format("{call %s(%s)}", spName, questionMaskString);
		
		return callSP;
	}
	
	private void insert(String insertStatement, ArrayList<ArrayList<String>> rows) throws SQLException {
		try (Connection connection = dataSource.getConnection()) {
			//Disable auto-commit
			connection.setAutoCommit(false);
			//Set save-point
			Savepoint savepoint = connection.setSavepoint();
			//Execute
			try (PreparedStatement preparedStatement = connection.prepareStatement(insertStatement)) {
				for (int i = 0; i < rows.size(); i++) {
					ArrayList<String> row = rows.get(i);
					for (int j = 0; j < row.size(); j++) {
						preparedStatement.setString(j + 1, row.get(j));
					}
					preparedStatement.addBatch();
					if ((i + 1) % insertBatchSize == 0) {
						preparedStatement.executeBatch();
						connection.commit();
						logger.trace("Inserted {}", i + 1);
					}
				}
				if (rows.size() % insertBatchSize != 0) {
					preparedStatement.executeBatch();
					connection.commit();
					logger.trace("Inserted {}", rows.size());
				}
				connection.commit();
			} catch (SQLException ex) {
				logger.debug(ex.getMessage(), ex);
				logger.debug("Rolling back ...");
				connection.rollback(savepoint);
				logger.debug("Rolling back done");
				throw ex;
			} finally {
				connection.setAutoCommit(true);
			}
		}
	}

	@Override
	public void insert(String table, ArrayList<String> fields,
			ArrayList<ArrayList<String>> rows) {
		// Check if inserted data is empty
		if (table == null || rows == null || rows.size() == 0) {
			return;
		}

		// Build Insert-Statement
		String insertStatement = buildInsertStatement(table,
				rows.get(0).size(), fields);
		logger.info(insertStatement);
		
		// Execute Insert-Statement
		try {
			insert(insertStatement, rows);
		} catch (SQLException e) {
			logger.error("Error in inserting. Error message: " + e.getMessage());
		}
	}

	@Override
	public void callSP(String spName, int amountParameters, List<?> lstData) {
		String callSP = buildCallableStatement(spName, amountParameters);
		System.out.println("callSP: " + callSP);
		
		try (Connection connection = dataSource.getConnection()) {
			//Disable auto-commit
			connection.setAutoCommit(false);
			//Set save-point
			Savepoint savepoint = connection.setSavepoint();
			
			//Execute
			try (CallableStatement cs = connection.prepareCall(callSP)) {
				
				Long[] urlIds = new Long[insertBatchSize];
				String[] urls = new String[insertBatchSize];
				Integer[] topicId1s = new Integer[insertBatchSize];
				Double[] weight1s = new Double[insertBatchSize];
				Integer[] topicId2s = new Integer[insertBatchSize];
				Double[] weight2s = new Double[insertBatchSize];
				Integer[] topicId3s = new Integer[insertBatchSize];
				Double[] weight3s = new Double[insertBatchSize];
				
				for (int i = 0; i < lstData.size(); i++) {
					DataStructure data = (DataStructure) lstData.get(i);
					
					urlIds[i] = data.getUrlId();
					urls[i] = data.getUrl();
					topicId1s[i] = data.getTopicId1();
					weight1s[i] = data.getWeight1();
					topicId2s[i] = data.getTopicId2();
					weight2s[i] = data.getWeight2();
					topicId3s[i] = data.getTopicId3();
					weight3s[i] = data.getWeight3();
					
				}
				
				ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor("T_ARRNUM", connection);
	            ARRAY array_to_pass = new ARRAY(descriptor, connection, urlIds);

				
				String [] northEastRegion = { "10022", "02110", "07399" };
				Array test = connection.createArrayOf("VARCHAR", northEastRegion); 
			
				Array changeUrlIds = connection.createArrayOf("T_ARRNUM", urlIds);
				cs.setArray(1, changeUrlIds);
				
				Array changeUrls = connection.createArrayOf("Array", urlIds);
				cs.setArray(2, changeUrls);
				
				Array changeTopic1 = connection.createArrayOf("Array", topicId1s);
				cs.setArray(2, changeTopic1);
				Array changeWeight1 = connection.createArrayOf("Array", weight1s);
				cs.setArray(1, changeWeight1);
				
				Array changeTopic2 = connection.createArrayOf("Array", topicId2s);
				cs.setArray(2, changeTopic2);
				Array changeWeight2 = connection.createArrayOf("Array", weight2s);
				cs.setArray(1, changeWeight2);
				
				Array changeTopic3 = connection.createArrayOf("Array", topicId3s);
				cs.setArray(2, changeTopic3);
				Array changeWeight3 = connection.createArrayOf("Array", weight3s);
				cs.setArray(1, changeWeight3);
				
				
//				if (lstData.size() % insertBatchSize != 0) {
//					cs.executeBatch();
//					connection.commit();
//					logger.trace("Inserted {}", lstData.size());
//				}
//				connection.commit();
				
				cs.execute();
				connection.commit();
				
				
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.debug(ex.getMessage(), ex);
				logger.debug("Rolling back ...");
				connection.rollback(savepoint);
				logger.debug("Rolling back done");
				throw ex;
			} finally {
				connection.setAutoCommit(true);
			}
		} catch (SQLException e) {
			logger.error("Error in call SP. Error message: " + e.getMessage());
		}
	}
}
