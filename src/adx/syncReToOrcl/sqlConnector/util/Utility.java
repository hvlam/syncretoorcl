/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author LamHV
 */
public class Utility
{

	public static ArrayList<String> transformToArrayList(String arg, String delimiter)
    {
        ArrayList<String> result = new ArrayList<String>();
        arg = arg.replaceAll("\\s", "");
        StringTokenizer t = new StringTokenizer(arg, delimiter);
        while (t.hasMoreTokens())
        {
            result.add(t.nextToken());
        }

        return result;
    }
}
