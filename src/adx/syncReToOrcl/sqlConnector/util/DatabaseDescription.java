/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.util;

/**
 *
 * @author LamHV
 */
public class DatabaseDescription
{
   private final String jdbcUrl;
   private final String user;
   private final String password;
   private int minPoolSize;
   private int maxPoolSize;
   private int acquireIncrement;
   private int maxStatements;
   
   public DatabaseDescription(String jdbcUrl, 
           String user, 
           String password,
           int minPoolSize, 
           int maxPoolSize,
           int acquireIncrement,
           int maxStatements)
   {
       this.jdbcUrl = jdbcUrl;
       this.user = user;
       this.password = password;
       this.minPoolSize = minPoolSize;
       this.maxPoolSize = maxPoolSize;
       this.acquireIncrement = acquireIncrement;
       this.maxStatements = maxStatements;
   }

    public int getAcquireIncrement()
    {
        return acquireIncrement;
    }

    public String getJdbcUrl()
    {
        return jdbcUrl;
    }

    public int getMaxPoolSize()
    {
        return maxPoolSize;
    }

    public int getMaxStatements()
    {
        return maxStatements;
    }

    public String getPassword()
    {
        return password;
    }

    public int getMinPoolSize()
    {
        return minPoolSize;
    }

    public String getUser()
    {
        return user;
    }
   
}
