/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.util;

import java.util.ArrayList;

/**
 *
 * @author root
 */
public class DataStructure
{
    private String id;
    private ArrayList<ArrayList<String>> rows;
    private String transcript;
    private String audioUrl; 
    
    public DataStructure()
    {
        this.rows = new ArrayList<ArrayList<String>>();
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setRows(ArrayList<ArrayList<String>> rows)
    {
        this.rows = rows;
    }

    public void setTranscript(String transcript)
    {
        this.transcript = transcript;
    }

    public void setAudioUrl(String audioUrl)
    {
        this.audioUrl = audioUrl;
    }

    public String getId()
    {
        return id;
    }

    public ArrayList<ArrayList<String>> getRows()
    {
        return rows;
    }

    public String getTranscript()
    {
        return transcript;
    }

    public String getAudioUrl()
    {
        return audioUrl;
    }
}
