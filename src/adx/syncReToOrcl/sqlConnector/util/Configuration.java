/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author LamHV
 */
public class Configuration
{
    private HashMap<String, Properties> connectionInfos;
    private DocumentBuilderFactory documentBuilderFactory;
    private Document document;
    private XPath xPath;
    private static Logger logger  = LoggerFactory.getLogger("system.Configurator.DB");
    private HashMap<String, ArrayList<String>> tables;
    private List<String> tableNames;

    public HashMap<String, Properties> getConnectionInfos()
    {
        return connectionInfos;
    }

    public HashMap<String, ArrayList<String>> getTables()
    {
        return tables;
    }
    
    public List<String> getTableNames() 
    {
    	return this.tableNames;
    }

    public Configuration()
    {
        this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
        this.xPath = XPathFactory.newInstance().newXPath();
        this.connectionInfos = new HashMap<String, Properties>();
        this.tables = new HashMap<String, ArrayList<String>>();
        this.tableNames = new ArrayList<String>();
    }

    public void loadConfig(String folderConfig)
    {
    	try {
    		String configPath = folderConfig + "/configDB.xml";
    		this.document = this.documentBuilderFactory.newDocumentBuilder().parse(configPath);

            // get connections for mysql
            String fieldsPath = "/root/Connectors/MysqlConnector";

            String jdbcUrl = getValue(fieldsPath + "/JdbcUrl");
            String user = getValue(fieldsPath + "/User");
            String password = getValue(fieldsPath + "/Password");
            String minPoolSize = getValue(fieldsPath + "/MinPoolSize");
            String maxPoolSize = getValue(fieldsPath + "/MaxPoolSize");
            String acquireIncrement = getValue(fieldsPath + "/AcquireIncrement");
            String maxStatements = getValue(fieldsPath + "/MaxStatements");

            Properties mysqlPros = new Properties();
            mysqlPros.put("jdbcUrl", jdbcUrl);
            mysqlPros.put("user", user);
            mysqlPros.put("password", password);
            mysqlPros.put("minPoolSize", minPoolSize);
            mysqlPros.put("maxPoolSize", maxPoolSize);
            mysqlPros.put("acquireIncrement", acquireIncrement);
            mysqlPros.put("maxStatements", maxStatements);
            
            connectionInfos.put("mysql", mysqlPros);
            
            // get connections for oracle
            fieldsPath = "/root/Connectors/OracleConnector";

            String ojdbcUrl = getValue(fieldsPath + "/OjdbcUrl");
            user = getValue(fieldsPath + "/User");
            password = getValue(fieldsPath + "/Password");
            minPoolSize = getValue(fieldsPath + "/MinPoolSize");
            maxPoolSize = getValue(fieldsPath + "/MaxPoolSize");
            acquireIncrement = getValue(fieldsPath + "/AcquireIncrement");
            maxStatements = getValue(fieldsPath + "/MaxStatements");

            Properties oraclePros = new Properties();
            oraclePros.put("jdbcUrl", ojdbcUrl);
            oraclePros.put("user", user);
            oraclePros.put("password", password);
            oraclePros.put("minPoolSize", minPoolSize);
            oraclePros.put("maxPoolSize", maxPoolSize);
            oraclePros.put("acquireIncrement", acquireIncrement);
            oraclePros.put("maxStatements", maxStatements);
            
            connectionInfos.put("oracle", oraclePros);
            
            fieldsPath = "/root/Tables";
            NodeList fieldNodes = getChildNodes(fieldsPath);
            
            for (int i = 0; i < fieldNodes.getLength(); i++)
            {
                String tableIds = fieldNodes.item(i).getNodeName();
                String tablePath = fieldsPath + "/" + tableIds;

                String tableName = getValue(tablePath + "/TableName");
                String fields = getValue(tablePath + "/Fields");
                
                this.tables.put(tableName, Utility.transformToArrayList(fields, ","));
                this.tableNames.add(tableName);
            }
			
		} catch (Exception e) {
			logger.error("Error in loading config file. Error message: " + e.getMessage());
		}
   
    }

    private String getValue(String path) throws XPathExpressionException
    {
        XPathExpression expr = xPath.compile(path);
        Object result = expr.evaluate(this.document, XPathConstants.NODE);
        Node node = (Node) result;
        return node.getAttributes().item(0).getNodeValue();
    }

    private String getChildNodeName(String path) throws XPathExpressionException
    {
        XPathExpression expr = xPath.compile(path + "/*");
        Object result = expr.evaluate(this.document, XPathConstants.NODE);
        Node node = (Node) result;
        return node.getNodeName();
    }

    private NodeList getChildNodes(String path) throws XPathExpressionException
    {
        XPathExpression expr = xPath.compile(path + "/*");
        Object result = expr.evaluate(this.document, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        return nodes;
    }
    
}
