/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author LamHV
 */
public class Common
{
    private static HashMap<String, ArrayList<String>> tables;
    
    public static ArrayList<String> getFields(String tableName)
    {
        if (tables != null)
        {
            return tables.get(tableName);
        }
        
        return null;
    }
    
    public static void setTables(HashMap<String, ArrayList<String>> tables)
    {
        Common.tables = tables;
    }
}
