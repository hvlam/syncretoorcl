/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.exception;

/**
 *
 * @author root
 */
public class DriverNotFoundException extends Exception
{
    public DriverNotFoundException(String message)
    {
        super(message);
    }
    
    public DriverNotFoundException(Throwable cause)
    {
        super(cause);
    }
    
    public DriverNotFoundException(String messange, Throwable cause)
    {
        super(messange, cause);
    }
}
