package adx.syncReToOrcl.sqlConnector.api;

import java.util.ArrayList;
import java.util.List;

public interface Insertor {
	void insert(String table, ArrayList<String> fields, ArrayList<ArrayList<String>> rows);
	void setInsertBatchSize(Integer insertBatchSize);
	void callSP(String spName, int amountParameters, List<?> lstData);
}
