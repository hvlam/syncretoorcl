/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.api;

import adx.syncReToOrcl.sqlConnector.impl.DatabaseManagerImpl;

/**
 *
 * @author root
 */
public class DatabaseManagers
{
    private static DatabaseManager databaseManager = null;
    
    private DatabaseManagers()
    {
    }
    
    public static DatabaseManager getDefaultDatabaseManager() 
    {
        if (databaseManager == null)
        {            
            DatabaseManagerImpl databaseManagerImpl = new DatabaseManagerImpl();
            databaseManagerImpl.setBatchSize(1000);
            databaseManagerImpl.initiate();
            databaseManager = databaseManagerImpl;
        }
        return databaseManager;
    }
}