/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.api;

import adx.syncReToOrcl.sqlConnector.exception.DriverNotFoundException;
import adx.syncReToOrcl.sqlConnector.util.DatabaseDescription;

/**
 *
 * @author root
 */
public interface DatabaseManager
{
    Selector createSelector(final String databaseLabel);
    Insertor createInsertor(final String databaseLabel);
    
    String connect(final DatabaseDescription databaseDecription, String dbType) throws DriverNotFoundException;
    
    void disconnect(final String databaseLabel);
    
    void shutdown();
}
