/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adx.syncReToOrcl.sqlConnector.api;

import java.util.ArrayList;

import adx.syncReToOrcl.sqlConnector.util.DataStructure;

/**
 *
 * @author root
 */
public interface Selector
{
    ArrayList<ArrayList<String>> getEasyId(String id, String tableName, int type);
    ArrayList<DataStructure> getHardId(String id, String tableName, int type);
    ArrayList<ArrayList<String>> getRandomId(ArrayList<String> ids, String tableName);
    ArrayList<ArrayList<String>> getImageId(String id, String tableName);
    ArrayList<ArrayList<String>> getAudioId(String id, String tableName);
    ArrayList<ArrayList<String>> countRealTest();
    ArrayList<ArrayList<String>> getRealTest(String tableName, String id);
    
    // Added
    ArrayList<ArrayList<String>> selectAllFields(String query, String tableName);
}