package adx.syncReToOrcl.sqlConnector.workaround;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import adx.syncReToOrcl.processor.DataStructure;

public class DatabaseService {
	private static Logger logger = LoggerFactory
			.getLogger("system.WorkAround.DataService");
	
	private static Connection connection;
	private static Properties pros;
	private static int numberOfReConnect = 0;
	
	public static void init(Properties argPros) {
		DatabaseService.pros = argPros;
		
		getConnection();
	}

	private static void getConnection() {
		if (connection != null) {
			return;
		}
		
		try {

			connection = DriverManager.getConnection(pros.getProperty("jdbcUrl"), 
					pros.getProperty("user"), 
					pros.getProperty("password"));

		} catch (SQLException e) {

			logger.error("Create connection failed...........");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error(String.format("PrintStackTrace: %s", errors.toString()));
		}
	}

	public static void callSP(String spName,
			List<?> lstData) {
		int insertBatchSize = lstData.size();
		Long[] urlIds = new Long[insertBatchSize];
		String[] urls = new String[insertBatchSize];
		Integer[] topicId1s = new Integer[insertBatchSize];
		Double[] weight1s = new Double[insertBatchSize];
		Integer[] topicId2s = new Integer[insertBatchSize];
		Double[] weight2s = new Double[insertBatchSize];
		Integer[] topicId3s = new Integer[insertBatchSize];
		Double[] weight3s = new Double[insertBatchSize];

		for (int i = 0; i < lstData.size(); i++) {
			DataStructure data = (DataStructure) lstData.get(i);

			urlIds[i] = data.getUrlId();
			urls[i] = data.getUrl();
			topicId1s[i] = data.getTopicId1();
			weight1s[i] = data.getWeight1();
			topicId2s[i] = data.getTopicId2();
			weight2s[i] = data.getWeight2();
			topicId3s[i] = data.getTopicId3();
			weight3s[i] = data.getWeight3();

		}

		try {

			ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor(
					"T_ARRNUM", connection);
			ArrayDescriptor descriptor1 = ArrayDescriptor.createDescriptor(
					"T_ARRCHAR", connection);

			ARRAY aUrlIds = new ARRAY(descriptor, connection, urlIds);
			ARRAY aUrls = new ARRAY(descriptor1, connection, urls);
			ARRAY aTopic1s = new ARRAY(descriptor, connection, topicId1s);
			ARRAY aWeight1s = new ARRAY(descriptor, connection, weight1s);
			ARRAY aTopic2s = new ARRAY(descriptor, connection, topicId2s);
			ARRAY aWeight2s = new ARRAY(descriptor, connection, weight2s);
			ARRAY aTopic3s = new ARRAY(descriptor, connection, topicId3s);
			ARRAY aWeight3s = new ARRAY(descriptor, connection, weight3s);

			String callSP = "call UPD_DETECT_TOPICS(?,?,?,?,?,?,?,?)";

			CallableStatement cs = connection.prepareCall(callSP);
			cs.setArray(1, aUrlIds);
			cs.setArray(2, aUrls);
			cs.setArray(3, aTopic1s);
			cs.setArray(4, aWeight1s);
			cs.setArray(5, aTopic2s);
			cs.setArray(6, aWeight2s);
			cs.setArray(7, aTopic3s);
			cs.setArray(8, aWeight3s);

			cs.executeUpdate();
			
		} catch (SQLException e) {
			logger.error("Call store procedure failed...........");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error(String.format("PrintStackTrace: %s", errors.toString()));
			
			logger.info("Reconnect...........");
			numberOfReConnect ++;
			if (numberOfReConnect < 5) {
				getConnection();
				callSP(spName, lstData);
			}
		}
	}
}
