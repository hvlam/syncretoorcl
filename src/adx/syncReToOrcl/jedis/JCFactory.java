package adx.syncReToOrcl.jedis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 *
 * @author LamHV
 */
public class JCFactory {
	// "jedisPool" is used for managing about connections to Redis server
	private static JedisPool jedisPool = null;
	private static Logger logger = LoggerFactory.getLogger("system.JCFactory");

	private static void initiatePool(JCConfiguration jcConfiguration) {
		JedisPoolConfig config = new JedisPoolConfig();
		// maximum active connections to Redis instance
		config.setMaxTotal(8);

		// Test whether connection is dead when connection retrieval method is
		// called
		config.setTestOnBorrow(true);

		// Test whether connection is dead when returning a connection to the
		// pool
		config.setTestOnReturn(true);

		// Number of connections to Redis that just sit there and do nothing
		config.setMaxIdle(2);

		// These can be seen as always open and ready to serve
		config.setMinIdle(1);

		// Tests whether connections are dead during idle periods
		config.setTestWhileIdle(true);

		// Maximum number of connections to test in each idle check
		config.setNumTestsPerEvictionRun(10);

		// Idle connection checking period
		config.setTimeBetweenEvictionRunsMillis(60000);

		JCFactory.jedisPool = new JedisPool(config,
				jcConfiguration.getHostIp(), jcConfiguration.getPort(), 0);

		logger.info("Initiating jedisPool.........");
	}

	public static JedisPool getInstance(JCConfiguration jcConfiguration) {
		if (jedisPool == null) {
			initiatePool(jcConfiguration);
		}

		return jedisPool;
	}

	public static Jedis getJedis() {
		logger.info("Get resource from jedisPool.....");
		
		return JCFactory.jedisPool.getResource();
	}

	public static void destroy() {
		if (JCFactory.jedisPool != null) {
			JCFactory.jedisPool.destroy();
		}
	}

}
