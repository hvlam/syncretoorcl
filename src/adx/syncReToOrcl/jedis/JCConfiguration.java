package adx.syncReToOrcl.jedis;

import adx.syncReToOrcl.util.ConfData;


/**
 *
 * @author LamHV
 */
public class JCConfiguration
{
    // Redis server's ip
    private String hostIp;
    
    // Redis server's port
    private int port;
     
    private String keyName;

    public JCConfiguration(ConfData confData)
    {
        this.hostIp = confData.getHostIp();
        this.port = confData.getPort();
    }

    public String getHostIp()
    {
        return hostIp;
    }

    public int getPort()
    {
        return port;
    }

    public String getKeyName()
    {
        return keyName;
    }

    public boolean isEqual(JCConfiguration jcConfiguration)
    {
        if (this.hostIp == jcConfiguration.getHostIp()
                && this.port == jcConfiguration.getPort())
//                && this.keyName == jcConfiguration.getKeyName())
        {
            return true;
        }
        return false;
    }
}
