package adx.syncReToOrcl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import adx.syncReToOrcl.jedis.JCConfiguration;
import adx.syncReToOrcl.jedis.JCFactory;
import adx.syncReToOrcl.processor.DataStructure;
import adx.syncReToOrcl.processor.Processor;
import adx.syncReToOrcl.sqlConnector.api.DatabaseManager;
import adx.syncReToOrcl.sqlConnector.api.DatabaseManagers;
import adx.syncReToOrcl.sqlConnector.api.Insertor;
import adx.syncReToOrcl.sqlConnector.api.Selector;
import adx.syncReToOrcl.sqlConnector.exception.DriverNotFoundException;
import adx.syncReToOrcl.sqlConnector.util.Common;
import adx.syncReToOrcl.sqlConnector.util.Configuration;
import adx.syncReToOrcl.sqlConnector.util.DatabaseDescription;
import adx.syncReToOrcl.sqlConnector.workaround.DatabaseService;
import adx.syncReToOrcl.util.ConfData;

public class App {
	private static Logger logger = LoggerFactory.getLogger("system.Main");
	
	public static void main(String[] args) {
		ConfData confData = adx.syncReToOrcl.util.Configuration.loadConfig(args[0]);
		
		// Initiating jedisPool
		JCConfiguration jcConfiguration = new JCConfiguration(confData);
		JedisPool jedisPool = JCFactory.getInstance(jcConfiguration);
		
		// Initiating database
		DatabaseManager dbManager = DatabaseManagers
				.getDefaultDatabaseManager();

		Configuration cf = new Configuration();
		cf.loadConfig(args[0]);

		HashMap<String, Properties> pros = cf.getConnectionInfos();
		Common.setTables(cf.getTables());
		
		// Information for mysql
		/*
		DatabaseDescription mysqlDbDesc = new DatabaseDescription(
				pros.get("mysql").getProperty("jdbcUrl"), pros.get("mysql").getProperty("user"),
				pros.get("mysql").getProperty("password"), Integer.parseInt(pros.get("mysql")
						.getProperty("minPoolSize")), Integer.parseInt(pros.get("mysql")
						.getProperty("maxPoolSize")), Integer.parseInt(pros.get("mysql")
						.getProperty("acquireIncrement")),
				Integer.parseInt(pros.get("mysql").getProperty("maxStatements")));
		
		String mysqlDatabaseId = null;
		try {
			mysqlDatabaseId = dbManager.connect(mysqlDbDesc);
		} catch (DriverNotFoundException ex) {
			logger.error("Error in ServletContextInitiator, " + "get clause: "
					+ ex.getCause() + "; get message: " + ex.getMessage());
		} 
		
		Selector selector = dbManager.createSelector(mysqlDatabaseId);
		
		String query = "select * from tb_urls_on_log_date limit 3";
		ArrayList<ArrayList<String>> rows = selector.selectAllFields(query, "tb_urls_on_log_date");
		
		System.out.println(rows);
		*/
		
		/*
		// Information for oracle
		DatabaseDescription orlcDbDesc = new DatabaseDescription(
				pros.get("oracle").getProperty("jdbcUrl"), 
				pros.get("oracle").getProperty("user"),
				pros.get("oracle").getProperty("password"), 
				Integer.parseInt(pros.get("oracle").getProperty("minPoolSize")), 
				Integer.parseInt(pros.get("oracle").getProperty("maxPoolSize")), 
				Integer.parseInt(pros.get("oracle").getProperty("acquireIncrement")),
				Integer.parseInt(pros.get("oracle").getProperty("maxStatements")));
		
		String oraclDatabaseId = null;
		try {
			oraclDatabaseId = dbManager.connect(orlcDbDesc, "oracle");
		} catch (DriverNotFoundException ex) {
			logger.error("Error in ServletContextInitiator, " + "get clause: "
					+ ex.getCause() + "; get message: " + ex.getMessage());
		} 
		
		List<DataStructure>	lst = null;
		try (Jedis jedis = jedisPool.getResource()) {
			lst = Processor.getKeys(jedis);
		}
		
		Insertor insertor = dbManager.createInsertor(oraclDatabaseId);
		insertor.setInsertBatchSize(200);
		insertor.callSP("UPD_DETECT_TOPICS", 8, lst);
		*/
		
//		Connection connection = DataService.getConnection(pros.get("oracle").getProperty("jdbcUrl"), 
//				pros.get("oracle").getProperty("user"), 
//				pros.get("oracle").getProperty("password"));
		
		List<DataStructure>	lst = null;
		try (Jedis jedis = jedisPool.getResource()) {
			DatabaseService.init(pros.get("oracle"));
			Processor.process(jedis);
		} catch (Exception e) {
			logger.error("Get resource from Redis failed...........");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error(String.format("PrintStackTrace: %s", errors.toString()));
		}
		
	}
}
