package adx.syncReToOrcl.processor;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import adx.syncReToOrcl.sqlConnector.util.Utility;
import adx.syncReToOrcl.sqlConnector.workaround.DatabaseService;
import redis.clients.jedis.Jedis;

public class Processor {
	private static Logger logger = LoggerFactory.getLogger("system.Processor");
	private static String keyPattern = "^(\\d)+$";

	public static void process(Jedis jedis) {

		List<DataStructure> lst = new ArrayList<DataStructure>();
		Set<String> keys = jedis.keys("*");

		int i = 0;
		for (String key : keys) {
			if (!key.matches(keyPattern)) {
				continue;
			}

			i++;
			logger.info("Key: " + key);

			Map<String, String> mapResult = jedis.hgetAll(key);
			if (mapResult != null) {
				DataStructure myDataStructure = new DataStructure();

				// get url
				String url = mapResult.get("url");
				myDataStructure.setUrl(url);

				// get topic-weight
				String topicWeight = mapResult.get("topic");

				if (topicWeight.equals("")) {
					logger.info("topicWeight: " + topicWeight);
					continue;
				}

				myDataStructure.setUrl(url);
				myDataStructure.setUrlId(Long.parseLong(key));

				ArrayList<String> arlTopicWeight = Utility
						.transformToArrayList(topicWeight, ",");
				for (int j = 0; j < arlTopicWeight.size(); j++) {
					ArrayList<String> tmp = Utility.transformToArrayList(
							arlTopicWeight.get(j), ":");
					Integer topicId = Integer.parseInt(tmp.get(0));
					Double weight = Double.parseDouble(tmp.get(1));

					switch (j) {
					case 0:
						myDataStructure.setTopicId1(topicId);
						myDataStructure.setWeight1(weight);
						break;
					case 1:
						myDataStructure.setTopicId2(topicId);
						myDataStructure.setWeight2(weight);
						break;
					case 2:
						myDataStructure.setTopicId3(topicId);
						myDataStructure.setWeight3(weight);
						break;
					}
				}

				lst.add(myDataStructure);
			}

			if (i % 10 == 0) {
				logger.info("Call as full batch size....");
				
				DatabaseService.callSP("UPD_DETECT_TOPICS", lst);

				lst.clear();
			}
		}

		if (lst.size() > 0) {
			logger.info("Call other remaining...");
			DatabaseService.callSP("UPD_DETECT_TOPICS", lst);
		}

	}
}
