package adx.syncReToOrcl.processor;

public class DataStructure {
	private Long urlId;
	private String url;
	private Integer topicId1;
	private Double weight1;
	private Integer topicId2;
	private Double weight2;
	private Integer topicId3;
	private Double weight3;
	public Long getUrlId() {
		return urlId;
	}
	public void setUrlId(Long urlId) {
		this.urlId = urlId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getTopicId1() {
		return topicId1;
	}
	public void setTopicId1(Integer topicId1) {
		this.topicId1 = topicId1;
	}
	public Double getWeight1() {
		return weight1;
	}
	public void setWeight1(Double weight1) {
		this.weight1 = weight1;
	}
	public Integer getTopicId2() {
		return topicId2;
	}
	public void setTopicId2(Integer topicId2) {
		this.topicId2 = topicId2;
	}
	public Double getWeight2() {
		return weight2;
	}
	public void setWeight2(Double weight2) {
		this.weight2 = weight2;
	}
	public Integer getTopicId3() {
		return topicId3;
	}
	public void setTopicId3(Integer topicId3) {
		this.topicId3 = topicId3;
	}
	public Double getWeight3() {
		return weight3;
	}
	public void setWeight3(Double weight3) {
		this.weight3 = weight3;
	}
}
