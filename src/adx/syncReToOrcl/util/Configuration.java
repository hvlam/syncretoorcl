package adx.syncReToOrcl.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration {
private static Logger logger = LoggerFactory.getLogger("system.Configuration");
	
	public static ConfData loadConfig(String configFolderPath) {
		String configPath = configFolderPath + "/configSyncApp.properties";
		
		String host;
		Integer port;
//		String keyName;
		
		ConfData confData = new ConfData();
		
		// get rawlog folder
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(configPath));
			
			host = properties.getProperty("hostIp");
			confData.setHostIp(host);
			port = Integer.parseInt(properties.getProperty("port"));
			confData.setPort(port);
		} catch (IOException ex) {
			logger.debug("Error as loading confuguration file. Error message: ", ex.getMessage());
		}
		
		return confData;
	}
}
